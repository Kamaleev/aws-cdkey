FROM nginx:1.21.3-alpine

COPY html/index.html /usr/share/nginx/html

EXPOSE 80

ENTRYPOINT ["nginx", "-g", "daemon off;"]
