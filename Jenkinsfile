def awsCredentials = [[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: 'aws-creds']]

pipeline {
    agent any
    environment {
        AWS_ACCOUNT_ID="329645891015"
        AWS_DEFAULT_REGION="eu-central-1" 
        IMAGE_REPO_NAME="dockerimagesrepo"
        IMAGE_TAG="latest"
        REPOSITORY_URI = "${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com/${IMAGE_REPO_NAME}"
    }

    options {
        withCredentials(awsCredentials)
    }

    tools {nodejs "nodejs"}

    stages {       
        stage('Logging into AWS ECR') {
            steps {
                script {
                    sh "aws ecr get-login-password --region ${AWS_DEFAULT_REGION} | docker login --username AWS --password-stdin ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com"
                }           
            }
        }
        stage('Cloning Git') {
            steps {
                checkout([$class: 'GitSCM', branches: [[name: '*/main']], extensions: [], userRemoteConfigs: [[credentialsId: 'gitlab-creds', url: 'https://gitlab.com/Kamaleev/aws-cdkey.git']]])     
            }
        }
        stage('Building image') {
            steps{
                script {
                    dockerImage = docker.build "${IMAGE_REPO_NAME}:${IMAGE_TAG}"
                }
            }
        }
        stage('Pushing to ECR') {
            steps{  
                script {
                    sh "docker tag ${IMAGE_REPO_NAME}:${IMAGE_TAG} ${REPOSITORY_URI}:$IMAGE_TAG"
                    sh "docker push ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com/${IMAGE_REPO_NAME}:${IMAGE_TAG}"
                }
            }
        }
        stage('Deploy to Fargate') {
            agent {
                dockerfile {
                    filename 'Dockerfile-nginx'
                    args "-v /etc/passwd:/etc/passwd"
                }
            }
            steps{  
                script {
                    sh "rm -rf fargateapp && mkdir fargateapp"
                    dir("./fargateapp") {
                        sh "ls -la ../../../../"
                        //sh "chown -R 995:993 ../../../../.npm"
                        sh "npm --version"
                        sh "node --version"
                        sh "which npm"
                        sh "npm config ls"
                        sh "cdk init --language typescript"
                        sh "cp -f ../fargateapp-stack.ts ./lib"
                        sh "cdk deploy"
                    }
                }
            }
        }
    }
    post {
        always {
            cleanWs()
        }
    }
}
